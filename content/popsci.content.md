## Popular Science

### 2023

#### Faites des langues

As part of the international week at the Saint-Étienne IUT (Faites des langues), I presented my research work in English to students.
My presentation was entitled "Physical attacks on secure electronic devices", and included a demonstration of a power consumption side-channel attack on a PIN code verification algorithm.
The slides I used are available [here](assets/popsci/2022_Faites_des_langues.pdf).

#### European Researchers' Night

<center>![](assets/popsci/2023_logo_NEDC.png){width=30%}</center>

The 2023 edition of the European Researchers' Night took place in two parts.
In the morning, I went to the Lycée Alexandre Lacassagne in Lyon to give an introduction to public key cryptography, demonstrate a side-channel attack on a PIN code verification algorithm and present the job of an associate professor to secondary school students.
In the evening, I had a stand open to the public to talk about cybersecurity, where I invited people to play a card game I designed.
This game, available [here](assets/popsci/2023_NEDC.pdf), is played by two people and highlights the possibility of creating a very strong password that respects the classic rules for creating a password, is unique and easy to remember.
The general public is generally very sensitive to this issue, which concerns them directly.

### 2022

#### Faites des langues

As part of the international week at the Saint-Étienne IUT (Faites des langues), I presented my research work in English to students.
My presentation was entitled "Physical attacks on secure electronic devices", and included a demonstration of a power consumption side-channel attack on a PIN code verification algorithm.
The slides I used are available [here](assets/popsci/2022_Faites_des_langues.pdf).

### 2021

#### Forum des métiers scientifiques

After two years of inactivity due to COVID, I went back to popular science by taking part in the Forum des métiers scientifiques, organised by the Benjamin Malossane secondary school in Saint Jean en Royans.
I introduced the students to public key cryptography using boxes and padlocks, and then told them about my job as an assistant professor.

### 2019

#### Fête de la Science

For the 2019 edition of the Science Festival, we received third grade classes.
With my fellow research engineer Nathalie Bochard, we presented cryptography.
We started with Caesar cipher, before discussing the team's research topic of random number generation.
We presented a random number generator based on ring oscillators, which we attacked by harmonic injection on the power supply to synchronize the oscillators and thus bias the generated random numbers.

#### European Researchers' Night

For the first edition of [European Researchers' Night in Saint-Étienne](https://www.larotonde-sciences.com), I led two small workshops :

- in the afternoon, for 9th graders, a cryptanalysis of the Caesar cipher,
- in the evening, for everyone, a workshop about passwords,

Later on, along with five other researchers, we performed a theater play in which we talked about our research topics. For this, we had been coached by Sabrina from [Ateliers de la rue Raisin](https://www.rueraisin.org/), who designed and directed the play.

#### Impro'Sciences

Along with the improvisation troup [Ni pied ni clé](https://fr-fr.facebook.com/nipiednicle), we performed the "Impro'Sciences" show.
The audience could write down questions of their choice.
Then, the troup presented two answers, a correct one and a completely wrong one.
The audience then had to vote for the most convincing explanation.
After that, some scientific background about the question was briefly discussed.

#### Pint of Science

<center>[![](https://pintofscience.fr/resources/themes/pos18/images/logo.svg){width=25%}](https://pintofscience.fr/)</center>

For Pint of Science 2019, my colleague Damien Robissout and I presented the usage of artificial intelligence for cybersecurity.
After briefly talking about cryptography and neural networks, we did a hands-on demo.
Someone from the audience had to type a message, along with a 4-digit PIN which was used to encrypt the message.
A neural network that we had trained was fed with the power consumption of the microcontroler that performed the encryption in order to recover the 4-digit secret PIN.
We then used it to decrypt the secret message.

The slides we used are available [here](assets/popsci/2019_PoS.pdf).

### 2018

#### Fête de la Science

In order to discover the [Centre of Microelectronics in Provence](https://www.mines-stetienne.fr/en/research/5-research-academic-centers/center-of-microelectronics-in-provence/), the four research departments designed an escape game, called [Escape Clean Room](https://www.fetedelascience.fr/pid35201/fiche-evenement.html?identifiant=69859778). Participants had to solve six puzzles to escape the (simulated) clean room.
The puzzles, designed by the research departments, were the following:

- [Bioelectronics](https://www.mines-stetienne.fr/en/research/scientific-departments/department-of-bioelectronics-bel/) : find the secret message during the virtual visit of the clean room,
- [Flexible Electronics](https://www.mines-stetienne.fr/en/research/scientific-departments/flexible-electronics-department-fel/) : find RFID tags hidden in a room,
- [Manufacturing Sciences and Logistics](https://www.mines-stetienne.fr/en/research/scientific-departments/manufacturing-sciences-logistics-department-sfl/) : solve a travelling salesman problem, find the lier using an interference graph,
- [Secure Architectures and Systems](https://www.mines-stetienne.fr/recherche/departements/systemes-et-architectures-securises-sas/) : decrypt the message encrypted with Caesar cipher by frequency analysis, heat the room to a selected temperature measured by ultrasound.

#### Alkindi contest

<center>[![](https://images.weserv.nl/?url=concours-alkindi.fr/images/logo.png){width=30%}](https://concours-alkindi.fr/)</center>

The [Alkindi](https://concours-alkindi.fr/) contest is an introduction to cryptography for 7th, 8th and 9th graders, organised by [Animath](https://www.animath.fr/) and [France-ioi](http://www.france-ioi.org/).
Regional winners could visit a research facility.
We had PACA regional winners to visit the Centre of Microelectronics in Provence.
I presented them frequency cryptanalysis applied to Caesar cipher, both in theory and practice!

### 2017

#### Fête de la Science

Along with the improvisation troup [Ni pied ni clé](https://fr-fr.facebook.com/nipiednicle), we performed the "Impro'Sciences" show.
The audience could write down questions of their choice.
Then, the troup presented two answers, a correct one and a completely wrong one.
The audience then had to vote for the most convincing explanation.
After that, some scientific background about the question was briefly discussed.

#### Ramène ta science

<center>[![](https://www.echosciences-loire.fr/uploads/a_d/image/attachment/1005399141/xl_Bandeau.jpg){width=40%}](https://www.echosciences-loire.fr/annonces/ramene-ta-science)</center>

For [Ramène ta science 2017](https://www.univ-st-etienne.fr/fr/recherche/agenda-actualites/annee-2016-2017/ramene-ta-science.html), I started by running a workshop for middle school students.
In the "Draw my science" framework, I presented the Caesar cipher and how it could be cryptanalysed with a frequency analysis.
Two teams later practiced the attack, attacking one another's encrypted messages.

I then played in a theatre play, along with other researchers where we carried a criminal investigation using our scientific skills.
To this end, I presented [key exchange protocols](https://www.echosciences-loire.fr/articles/ma-science-en-180-secondes-par-brice).

### 2016

#### Fête de la Science

For [Fête de la Science 2016](https://www.univ-st-etienne.fr/fr/tous-les-faits-marquants/annees-precedentes/annee-2016-2017/zoom-sur/fete-de-la-science-25e-edition.html) at Jean Monnet University, I took part in a theatre play along with other researchers.
We were coached by professional actors and played a fake "Three Minute Thesis" contest, which did not go as planned!

### 2015

#### Science & You

<center>[![](https://images.weserv.nl/?url=www.science-and-you.com/sites/science-and-you.com/files/logo.png){width=30%}](https://www.science-and-you.com/en)</center>

The [Science & You](https://www.science-and-you.com/en) event was organised in June 2015 by [Lorraine University](https://welcome.univ-lorraine.fr/en).
I had the chance to present my PhD subject in drawings, cartoons more specifically.
We were inspired by the [Pr. Schmitt teleporter](https://pebfox.com/blog) comic and were coached by the authors Peb & Fox.
We had a quick training before applying these new techniques to our work.
We then presented the result to the general public in Nancy.
