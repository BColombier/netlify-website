<aside>![](assets/me.png){alt="me" width="11em"}</aside>

Since 2022, I am an associate professor at [Université Jean Monnet](https://www.univ-st-etienne.fr/en) in Saint-Étienne.
I do research in [Laboratoire Hubert Curien](https://laboratoirehubertcurien.univ-st-etienne.fr) in the [SESAM team](https://laboratoirehubertcurien.univ-st-etienne.fr/en/teams/secure-embedded-systems-hardware-architectures.html) and I teach electronics and programming in the [GEII department](https://www.iut.univ-st-etienne.fr/fr/etudier-a-l-iut-de-saint-etienne/nos-formations/les-b-u-t/b-u-t-genie-electrique-et-informatique-industrielle.html) at [IUT Saint-Étienne](https://www.iut.univ-st-etienne.fr/en).

From 2020 to 2022, I was an associate professor at [Grenoble INP](https://www.grenoble-inp.fr/en).
I was doing research in the [TIMA laboratory](https://tima.univ-grenoble-alpes.fr/) in the [AMfoRS team](https://tima.univ-grenoble-alpes.fr/research/amfors) and teaching computer science at [Phelma](https://phelma.grenoble-inp.fr/en) engineering school.

From 2018 to 2020, I was a postdoc researcher in the [Secure Embedded Systems and Hardware Architectures](https://laboratoirehubertcurien.univ-st-etienne.fr/en/teams/secure-embedded-systems-hardware-architectures.html) team at [Laboratoire Hubert Curien](https://laboratoirehubertcurien.univ-st-etienne.fr/en/index.html) in Saint-Etienne, France, as part of the FUI-PILAS project.
I was working on advanced laser fault injection attacks that make use of multi-spot laser stations.

From 2017 to 2018, I was a postdoc researcher at [CEA-TECH DPACA](https://www.cea-tech.fr/cea-tech/Pages/en-regions/prtt-provence-alpes-cote-azur.aspx), in the [Secure Systems and Architectures](https://www.mines-stetienne.fr/recherche/centres-et-departements/systemes-et-architectures-securises-sas/) team in Gardanne, at the [Centre of Microelectronics in Provence Georges Charpak](https://www.mines-stetienne.fr/recherche/5-centres-de-formation-et-de-recherche/centre-microelectronique-de-provence/).
I was working on evaluating the resistance of software against physical attacks (side-channel and fault injection) as part of the [ANR-PROSECCO](https://wp-systeme.lip6.fr/prosecco/) project.

From 2014 to 2017, I was a Ph.D. student in the [Secure Embedded Systems and Hardware Architectures](https://laboratoirehubertcurien.univ-st-etienne.fr/en/teams/secure-embedded-systems-hardware-architectures.html) team at the [Laboratoire Hubert Curien](https://laboratoirehubertcurien.univ-st-etienne.fr/en/index.html) in Saint-Etienne, France.
My supervisors were Pr. [Lilian Bossuet](https://perso.univ-st-etienne.fr/bl16388h/) and Dr. [David Hély](https://lcis.fr/ctsys).
My thesis topic was the fight against counterfeiting and illegal copying of integrated circuits and intellectual property cores.
I worked as part of the [ANR-SALWARE](https://perso.univ-st-etienne.fr/bl16388h/salware/) project and was funded by the [Auvergne-Rhône-Alpes](https://www.auvergnerhonealpes.fr/) region.
You can find my [PhD thesis](/assets/PhD/thesis.pdf) and the [defense slides](/assets/PhD/defense_slides.pdf) here.
