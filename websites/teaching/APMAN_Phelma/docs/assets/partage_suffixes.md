---
title: Partage des suffixes
geometry: margin=4cm
pagestyle: empty
papersize: A4
---


L'arbre radix permet de partager les **préfixes** des mots du dictionnaire, et donc de gagner de la place en mémoire. Les mots commençant par la même sous-chaîne initiale se partagent un chemin identique au début de l'arbre.

Il est possible de réduire l'espace mémoire occupé par un arbre radix en partageant les sous-arbres communs, c'est à dire les **suffixes** que les mots ont en commun.
Chaque fois que deux sous-arbres sont identiques, l'un d'entre eux peut être détruit et remplacé par l'autre.

Cette structure de données est en réalité un graphe, DAG pour *Directed Acyclic Graph*.
Dans le cas de notre dictionnaire, la recherche de mots s'effectue sans aucune modification, trouver un mot dans le dictionnaire revient à rechercher un chemin entre la racine de l'arbre et une feuille.

Il s'agit donc de compresser le dictionnaire grâce à une fonction de compression.
Cette fonction prend un arbre non compressé en argument et retourne un arbre compressé.
Elle est **récursive** et réalise les actions suivantes :

1. Elle compresse le fils droit,
2. Elle compresse le fils gauche,
3. Elle cherche dans une structure de données des suffixes déjà rencontrés (à définir par vous même) si un sous-arbre avec la même lettre et les mêmes fils a déjà été rencontré, c'est à dire si le suffixe formé par le noeud en cours de compression et ses fils sont un suffixe déjà rencontré.
4. Si ce sous-arbre existe déjà, elle détruit le noeud (mais pas les fils) en cours de compression et retourne le sous-arbre déjà rencontré,
5. Sinon, elle rajoute à la structure de données des suffixes déjà rencontrés et retourne le noeud compressé.

Cet algorithme est très efficace. Il permet de supprimer plus de 90% des noeuds sur le dictionnaire français fourni, avec un temps de recherche identique à la version non compressée.
La structure de données des suffixes déjà rencontrés est un élément clé de la rapidité de cette fonction de compression.
Le nombre de suffixes est en effet très rapidement de grande taille.
Si vous utilisez une structure de donnée avec recherche linéaire, le temps de calcul sera rapidement prohibitif sur un dictionnaire réel.
Choisissez donc bien cette structure.

**Remarque** : faites attention à la notion d'égalité des fils à l'étape 3. Pensez bien qu'à cette étape, les fils du noeud à compresser ont déjà été compressés. Cela permet d'optimiser cette notion de *même fils*.
