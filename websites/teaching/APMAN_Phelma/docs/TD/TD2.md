---
title: TD2 - Traitement séquentiel
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - utiliser des structures de contrôle de base (boucles, branchement conditionnel)
    - réfléchir à la complexité des implémentations

#### Exercice 1 : séquence d'entiers traités à la volée

!!! info "Minimum requis"
    Faire au minimum les questions 1, 2 et 3.
    
On lit une séquence d'entiers au clavier en effectuant un traitement **à la volée** dessus.
Pour terminer la saisie, il faut choisir une valeur qui sert de **marque de fin de séquence**.
Ici, on lira par exemple des suites d'entiers terminées par `-1`.
Les problèmes traitables à la volée sont ceux pour lesquels la mémoire nécessaire est statiquement bornée.
La forme des programmes à écrire est ici toujours la suivante : 

``` c
void traitement_sequentiel()
{
  int courant; /* l'entier lu */
  /* ... variables servant au calcul */

  scanf("%d", &courant);
  /* ... initialisation des variables */
  while (courant != -1) {
    /* ... traitement a l'interieur de la boucle */
    scanf("%d", &courant);
  }
  /* ... traitement final */
  printf(...); /* affichage du resultat */
}
```

1. Afficher la somme des entiers lus en entrée.
2. Indiquer si la suite des entiers est croissante.
3. Afficher le maximum des entiers lus.
4. Déterminer la position de la première occurence du maximum.
5. Déterminer la position de la dernière occurence du maximum.
6. Calculer le nombre d'occurrences du maximum.


#### Exercice 2 : séquence de caractères

!!! info "Minimum requis"
    Faire au minimum les questions 1, 2, 3 et 4.
    
Même exercice mais avec des caractères.
Le caractère de fin de séquence est '#'.

A noter, si vous utilisez le squelette de code de l'exercice précédent : pour `scanf`, le saut de ligne (taper sur la touche 'entrée') compte comme un caractère.

Pour faire le test, on pourra taper : "RAdAr" (nombre de lettres = 5 ; nombre de A = 2)

Ou (nombre de lettres = 10 ; nombre de A = 2) :

``` terminal
R
A
d
A
r
```

1. Compter le nombre de 'A' (sans mémoire autre que le nombre de 'A')
2. Compter le nombre de "LE" (avec la mémoire minimale, c'est-à-dire un booléen, attention à l'initialisation de l'algorithme)
3. Compter le nombre de mots (même idée que "compter les LE")
4. Compter le nombre de mots terminés par 'X' (attention à la fin du traitement)
5. Calculer la moyenne de la longueur des mots (avec une seule boucle)
6. Compter le nombre de nombres en base 10 (attention, ici, le texte en entrée peut comporter des caractères qui ne sont pas nécessairement des chiffres)
7. Calculer la somme des nombres (en base 10) rencontrés.

#### Exercice 3 : suite de Fibonacci

**Définition :**

  * $\phi_0 = 0$ ; $\phi_1 = 1$
  * pour tout entier positif $n \ge 2$ : $\phi_n = \phi_{n-1} + \phi_{n-2}$

Les premiers éléments de la suite sont donnés au [lien suivant](https://oeis.org/A000045).

Le prototype de la fonction associée est le suivant :

``` c
long fibonacci(unsigned n);
```

1. Écrire une fonction **récursive** qui calcule $\phi_n$ pour une valeur de $n$ **passée en paramètre à l'exécutable**.
2. Étudier la complexité en temps et en mémoire de la fonction.
3. **Mesurer** le temps d'exécution pour différentes valeurs de $n$ (avec la commande `/bin/time` ou `gprof`, voir [TD1](../TD1/#profiling)).
   Tracer le graphe du temps d'exécution en fonction de $n$, puis comparer aux complexités précédemment identifiées.
4. Écrire une fonction **itérative** qui calcule $\phi_n$ pour une valeur de $n$ passée en paramètre.
5. Se reposer les questions 2 et 3 pour cette version itérative.

#### Exercice 4 : évaluation de polynôme

Ecrire une fonction qui étant donnée la valeur d'une inconnue $x$ et un entier $n$, demande à l'utilisateur les $n+1$ coefficients $a_0$, $a_1$, ..., $a_n$ d'un polynôme de degré $n$ et affiche le résultat de l'évaluation du polynôme au point $x$ :

$$
P(x) = a_0 + a_1 \times x + a_2 \times x^2 + ... + a_n \times x^n
$$

Le prototype de la fonction est le suivant :

``` c
int eval_poly(int x, unsigned n);
```

Pour calculer cette valeur, on ne doit utiliser que des additions et des multiplications (**pas de fonction puissance**).

1. Faire une première version où les coefficients sont demandés dans l'ordre croissant.
   Combien de multiplications et d'additions sont nécessaires, en fonction de $n$ ?
2. Faire une deuxième version où les coefficients sont demandés dans l'ordre décroissant.
   On utilisera [la méthode de Horner](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Ruffini-Horner#Valeur_d'un_polyn%C3%B4me_en_un_point) pour évaluer le polynôme :

$$
P(x) = a_0 + x \times (a_1 + x \times (a_2 + x \times(...)))
$$
   
Combien de multiplications et d'additions sont nécessaires en fonction de $n$ ?
Concluez quant à la complexité respective des deux méthodes d'évaluation.
La méthode de Horner a un autre avantage, lié à la capacité des types représentant les nombres dans l'ordinateur.
Quel est cet autre avantage ?

#### Exercice 5 : multiplication

Le but de cet exercice est d'écrire une fonction de multiplication sans utiliser l'opérateur `*` :

``` c
unsigned mult(unsigned x, unsigned y);
```

1. Dans une première version, utilisez un algorithme d'additions successives. Combien d'additions nécessite une multiplication dans cette version ?
2.  Dans une deuxième version, utilisez l'algorithme de multiplication vu à l'école primaire, en base 2 au lieu d'être en base 10.
    Plus précisément, en plus de l'opération d'addition, on utilise la multiplication par 2 et l'information de parité : si `b` est une variable de type `unsigned`, l'expression booléenne `(b%2 == 1)` est vraie si et seulement si `b` est impair.
    
    Dans le pire cas, combien d'additions nécessite une multiplication dans cette version ? 
