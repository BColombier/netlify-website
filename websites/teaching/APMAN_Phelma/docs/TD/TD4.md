---
title: TD4 - Listes chaînées
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - revoir la méthode d'implémentation d'une liste chaînée en C
    - implémenter les opérations de base sur les listes chaînées
    - terminer en ayant une implémentation de liste chaînée de référence pour les TD suivants

## Définition et usage

### Définition

On définit une liste chaînée d'entiers avec le type suivant :

``` c
struct cellule {
    int valeur;
    struct cellule* suivante;
};
typedef struct cellule* liste;
```

On définit un élément de la liste comme un type composé `struct cellule` qui comprend :

* un entier : `valeur`
* un pointeur vers un élément (de type composé `struct cellule`) : `suivante`

On définit ensuite un nouveau type `liste` (avec `typedef`) qui est équivalent à un pointeur vers le type composé `struct cellule`.

Ainsi, une liste est un **pointeur vers le premier élément** de cette liste.

On peut représenter la liste chaînée graphiquement de cette façon :

![liste-chainee](../images/liste_chainee.png "Liste chaînée")

### Usage

Pour construire une liste d'une cellule portant la valeur 30, puis la désallouer :

``` c
liste l = malloc(sizeof(struct cellule)); /* allocation de la cellule */

l->valeur = 30;
l->suivante = NULL; /* par defaut */

free(l); /* désallocation */
```

## Exercices

### Opérations sur les listes chaînées

On suppose ici qu'une liste vide est représentée par le pointeur `NULL`.

#### Exercice 1 : affichage

Écrire une fonction qui affiche une liste.

``` c
void affiche(liste l);
```
Quelle est la complexité en temps de cette fonction ?

#### Exercice 2 : insertion

1. Écrire une fonction qui insère un élément **en tête** d'un liste.
   ``` c
   void insere_tete(int nouveau, liste* pl);
   ```

Quelle est la complexité en temps de cette fonction ?

2. Écrire une fonction qui insère un élément **en queue** d'une liste.
    ``` c
    void insere_queue(int nouveau, liste* pl);
    ```

Quelle est la complexité en temps de cette fonction ?

#### Exercice 3 : recherche

Écrire une fonction qui indique si un élément est présent dans une liste.

``` c
bool recherche(int valeur, liste l);
```

Quelle est la complexité en temps de cette fonction ?

#### Exercice 4 : suppression

Écrire une fonction qui supprime un élément dans une liste.

``` c
void supprime(int valeur, liste* pl);
```

Quelle est la complexité en temps de cette fonction ?

#### Exercice 5 : inversion

Écrire une fonction qui inverse une liste.

``` c
void inverse(liste* pl);
```

!!! danger "Attention"
    Vous veillerez à ce que votre fonction d'inversion soit **efficace**, c'est à dire :
    
    * qu'elle ne réalise **pas d'allocation**
    * qu'elle s'exécute en temps **linéaire**

#### Exercice 6 : libération

Écrire une fonction qui libère une liste.

``` c
void libere(liste* pl);
```

### Représentations alternatives d'une liste chaînée

Il est possible de représenter une liste chaînée d'autres manières.
La liste vide n'est alors plus représentée par le pointeur `NULL`.

1. une liste avec une sentinelle en tête,
2. une liste circulaire,
3. une liste représentée par deux pointeurs, l'un sur le début, l'autre sur la fin
   ``` c
   typedef struct {
       struct cellule* tete;
       struct cellule* queue;
   } liste;
   ```

Coder les fonctions des exercices 1 à 5 en travaillant avec ces nouvelles représentations.
À chaque fois, réflechissez à l'influence de cette nouvelle représentation sur la complexité des opérations.
