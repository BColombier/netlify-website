---
title: TD6 - Tables de hachage
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - implémenter une table de hachage
    - vérifier que la propriété de vérification d'appartenance en temps constant est vérifiée

On décrit la table de hachage (*hash table* en anglais) par la structure de données suivante :

``` C
typedef struct {
  liste* table;
  unsigned capacite; /* capacité de la table */
  unsigned nb_elements; /* nombres d'éléments dans la table */
  unsigned capacite_initiale; /* utile lors du redimensionnement */
} table_hachage;
```

La `table` est un pointeur vers une liste chaînée d'éléments.
Son rôle est de stocker les élements ayant le même *hash* (ces éléments sont dit **en collision**).

``` C
typedef struct _cellule {
  T element;
  struct _cellule* suivante;
} cellule;
typedef cellule* liste;
```

Les élements stockés dans la table de hachage ont un type **générique** `T`.

Pour la mise en oeuvre de notre table de hachage, nous devons assigner un type réel au type générique.
Nous prendrons l'exemple suivant, permettant de stocker des coordonnées GPS sous sous la forme d'une paire de nombres décimaux (latitude, longitude):

``` C
typedef struct coordonnee {
  float lat;
  float lon;
} T;
```

#### Exercice 1 : implémentation

Implémenter les fonctions suivantes opérant sur la structure de données décrite ci-dessus :

  * Obtenir le *hash* d'un élément (vous pouvez ajouter des paramètres à cette fonction si nécessaire)
  ``` C
  int hash(T element);
  ```
  * Vérifier si deux éléments de type générique `T` sont égaux
  ``` C
  bool identiques(T element_1, T element_2);
  ```
  * Vérfier si un élément est présent dans la table de hachage, en réutilisant les deux fonctions précédentes
  ``` C
  bool est_present(T element, table_hachage* ht);
  ```
  * Insérer un nouvel élément dans la table de hachage (on réalisera une insertion en tête dans la liste, en ayant vérifié au préalable que l'élément à insérer n'est pas déjà présent)
  ``` C
  void inserer_sans_redimensionner(T element, table_hachage* ht);
  ```
  * Afficher le contenu de la table de hachage
  ``` C
  void afficher_table(table_hachage* ht);
  ```

#### Exercice 2 : évaluation

Une fois la table de hachage implémentée, évaluez ses performances.
Pour cela, mesurez le temps pris par les opérations d'insertion et de vérification de présence.
En particulier, évaluez comment ce temps évolue en fonction :

  - du taux de remplissage de la table
  - du nombre de conflits
  - de la fonction de hachage utilisée

Concluez en précisant sous quelles conditions la table de hachage fonctionne comme attendu, c'est à dire avec une vérification de présence en temps constant.
Pour cela, vous pouvez réfléchir aux meilleurs et pires cas pour les opérations décrites ci-dessus.
Vous pourrez par exemple réfléchir à une **mauvaise** fonction de hachage.

#### Exercice 3 : optimisation du fonctionnement

Ajouter une fonction permettant de redimensionner la table de hachage si elle dépasse un taux de remplissage de 2/3.
Observez l'impact de cette optimisation sur le temps pris par les fonctions évaluées à l'exercice précédent.

#### Exercice 4 : gestion des collisions par adressage ouvert

Jusqu'à présent, nous gérions les collisions (élément possédant le même *hash*) en stockant les éléments dans une liste chaînée.
Une autre option est de stocker les éléments dans une case vide qui suit.
Cette technique est appelée [adressage ouvert](https://fr.wikipedia.org/wiki/Table_de_hachage#Adressage_ouvert).

Notez l'économie en mémoire réalisée par cette technique, puis mettez-la en oeuvre.
