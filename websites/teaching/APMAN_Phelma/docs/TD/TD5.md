---
title: TD5 - Types abstraits de données
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - comprendre la notion de type abstrait de données
    - implémenter deux types abstraits de données simples : la pile et la file

### Piles

Une pile (*stack* en anglais, ou *LIFO : last in, first out*) est une structure de donnée pour laquelle sont définies deux fonctions :

  * empiler (*push* en anglais) : ajouter un élément sur la pile
  * dépiler (*pop* en anglais) : retirer un élément de la pile
  
Puisque c'est une pile, les éléments sont défilés **dans l'ordre inverse** duquel ils ont été empilés.

#### Exercice 1 : pile de taille bornée

On implémente ici une pile de taille bornée à l'aide d'un **tableau**.

1. Spécifier la structure de donnée `pile` implémentant une pile de taille bornée.
2. Sous quelles **précondition(s)** a-t-on le droit d'utiliser la fonction `empiler` ?  
   Sous quelles **précondition(s)** a-t-on le droit d'utiliser la fonction`depiler` ?

Implémenter ensuite les fonctions qui réalisent les actions suivantes.
Pour chacune d'elles, réfléchir aux préconditions et les vérifier à l'aide d'`assert`.

  * Créer une pile vide de taille `taille`
  ``` c
  pile* creer_pile_vide(unsigned taille);
  ```
  * Désallouer la pile
  ``` c
  void detruire_pile(pile* p);
  ```
  * Vérifier si la pile est pleine
  ``` c
  bool pile_est_pleine(pile* p);
  ```
  * Vérifier si la pile est vide
  ``` c
  bool pile_est_vide(pile* p);
  ```
  * Empiler un élément
  ``` c
  void empiler(pile* p, int valeur);
  ```
  * Dépiler un élément
  ``` c
  int depiler(pile* p);
  ```
  * Afficher les éléments de la pile
  ``` c
  void afficher_pile(pile* p);
  ```
  * Vider la pile
  ``` c
  void vider_pile(pile* p);
  ```

#### Exercice 2 : pile de taille variable

1. Spécifier la structure de donnée `pile` implémentant une pile de taille variable.
   On utilisera pour cela un **tableau de taille variable**.

2. Implémenter les fonctions de [l'exercice 1](#exercice-1-pile-de-taille-bornee) pour cette pile.  
   Quand le tableau est plein, prévoir d'agrandir le tableau en doublant sa taille.
   Quand le tableau est plein au quart, prévoir de diminuer sa taille de moitié.

Quel est le coût d'insertion dans la pile : dans le pire cas ? dans le meilleur cas ?
Quel est le coût **amorti** d'insertion dans la pile ?

### Files

Une file (ou *queue*, ou *FIFO : first in, first out*) est une structure de donnée pour laquelle sont définies deux fonctions :

  * enfiler (*enqueue* en anglais) : ajouter un élément dans la file
  * défiler (*dequeue* en anglais) : retirer un élément de la file
  
Puisque c'est une file, les éléments sont défilés **dans l'ordre** dans lequel ils ont été enfilés.

!!! info "Application"
    Les files sont utilisées, entre autre :

    * comme mémoire tampon (par exemple pour le stockage des documents en attente d'impression)
    * pour l'ordonnancement des tâches par le système d'exploitation
    * pour modéliser les stocks d'une entreprise

#### Exercice 3 : file de taille variable

1. Spécifier la structure de donnée `file` implémentant une file de taille variable.
   On utilisera notamment pour cela un **tableau de taille variable**.

2. Implémenter les fonctions de [l'exercice 1](#exercice-1-pile-de-taille-bornee) pour cette file.

Quel est le coût d'insertion dans la file : dans le pire cas ? dans le meilleur cas ?
Quel est le coût **amorti** d'insertion dans la file ?

#### Exercice 4 : file de taille variable implémentée par une liste chaînée

1. Spécifier la structure de donnée `file` implémentant une file de taille variable.
   On utilisera pour cela une **liste chaînée**.

2. Implémenter les fonctions de [l'exercice 1](#exercice-1-pile-de-taille-bornee) pour cette file.

!!! warning "Complexité"
    Attention à ce que la complexité des fonctions reste raisonnable.
