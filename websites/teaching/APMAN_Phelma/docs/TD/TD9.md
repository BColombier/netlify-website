---
title: TD9 - Arbres et récursivité
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - implémenter la structure de données d'arbre binaire
    - implémenter des fonctions récursives travaillant sur cette structure de données

#### Exercice 1 : structure de données

Proposer une structure de données adaptée permettant de représenter un arbre **binaire** (chaque nœud a deux éléments fils au plus).
Une piste consiste à partir d'une liste chaînée, et de réfléchir à ce qu'il faut y ajouter.

#### Exercice 2 : création et destruction

Implémenter les fonctions réalisant les actions suivantes :

  * Création d'un nœud
  ``` C
  noeud* creer_noeud(int valeur);
  ```
  * Vérification qu'un arbre est vide
  ``` C
  bool arbre_est_vide(arbre a);
  ```
  * Destruction **recursive** d'un arbre (avec libération correcte de la mémoire)
  ``` C
  void detruire_arbre(arbre a);
  ```

#### Exercice 3 : vérification de propriétés

Implémenter de manière récursive les fonctions vérifiant les propriétés suivantes sur un arbre :

  * Calcul de la hauteur de l'arbre (la profondeur maximale d'un nœud)
  ``` C
  unsigned hauteur(arbre a);
  ```
  * Calcul du nombre de nœuds de l'arbre
  ``` C
  unsigned nb_noeuds(arbre a);
  ```
  * Calcul du nombre de feuilles (nœuds sans fils) de l'arbre
  ``` C
  unsigned nb_feuilles(arbre a);
  ```
  * Tester si un arbre est **dégénéré** (filiforme)
  ``` C
  bool arbre_est_degenere(arbre a);
  ```

    !!! note "Note"
        Un arbre dégénéré peut être décrit par une liste chaînée.

    Exemple d'arbre filiforme :

    ![arbre-filiforme](../images/arbre_filiforme.png "Arbre filiforme")

  * Tester si un arbre est **parfait**. Un arbre est parfait si toutes les feuilles sont à la même distance de la racine.
  ``` C
  bool arbre_est_parfait(arbre a);
  ```

    Exemple d'arbre parfait :

    ![arbre-parfait](../images/arbre_parfait.png "Arbre parfait")

    !!! warning "Complexité"
        Attention à la **complexité** de votre implémentation. En particulier, l'approche consistant à vérifier que tous les sous-arbres ont la même hauteur est peu efficace.

#### Exercice 4 : parcours

Implémenter les fonctions permettant de parcourir l'arbre et d'en afficher les nœuds des trois manières suivantes :

  1. Parcours préfixé : on visite la racine, puis le fils gauche, puis le fils droit.

     ``` C
     void parcours_prefixe(arbre a);
     ```

  2. Parcours infixé : on visite le fils gauche, puis la racine, puis le fils droit.

     ``` C
     void parcours_infixe(arbre a);
     ```

  3. Parcours postfixé : on visite le fils gauche, puis le fils droit, puis la racine.

     ``` C
     void parcours_postfixe(arbre a);
     ```

Modifier ensuite la fonction de parcours infixé pour afficher un nombre $n$ d'espaces avant la valeur du nœud. $n$ sera incrémenté lors de la descente dans l'arbre.

#### Exercice bonus : parcours en largeur

Une autre possibilité pour afficher un arbre consiste à l'afficher "étage par étage" : c'est le **parcours en largeur**.
Implémenter la fonction suivante permettant de parcourir un arbre en largeur.

``` C
void parcours_largeur(arbre a);
```
!!!note "Structure de données"
    Vous aurez besoin d'une [file](TD5/#files).
