---
title: TD8 - Algorithmes de tri efficaces 
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - implémenter des algorithmes de tri efficaces
    - comparer plusieurs algorithmes réalisant la même fonction
    
!!! note "Vérification du bon fonctionnement"
    Suivre la [même procédure](../TD7/#verification-du-bon-fonctionnement) qu'au TD précédent pour vérifier le **bon fonctionnement** de vos algorithmes de tri

#### Exercice 1 : tri fusion

Implémenter de manière **récursive** l'algorithme de tri fusion.
Il fonctionne de la manière suivante :

- Découper **résursivement** le tableau en deux parties, jusqu'à obtenir des **case uniques**
    - Remarquer que ces cases uniques constituent des tableaux **triés**
- **Fusionner** les paires de tableaux, en remontant jusqu'à la taille initiale du tableau
    - Prendre **au choix** les éléments du tableau 1 ou du tableau 2 pour ne former qu'un seul tableau, **trié**

``` c
void tri_fusion(int tab[], unsigned taille);
```

#### Exercice 2 : tri par segmentation

Implémenter de manière **récursive** l'algorithme de tri par segmentation (également appelé tri rapide, tri pivot ou *quicksort*).
Il fonctionne de la manière suivante :

- Choisir un élément du tableau (le **pivot**) et le placer à la **fin** du tableau
- Placer au **début** du tableau les éléments **plus petits** que le pivot : c'est l'ensemble $E_{petits}$
- Placer à la **fin** du tableau les éléments **plus grands** que le pivot : c'est l'ensemble $E_{grands}$
- Placer le pivot **entre** $E_{petits}$ et $E_{grands}$
- Recommencer la procédure sur $E_{petits}$ et $E_{grands}$ jusqu'à ce qu'ils soient de **taille 1**

``` c
void tri_segmentation(int tab[], unsigned taille);
```

#### Exercice 3 : comparaison

Comparer les temps d'exécutions des deux algorithmes précédents pour des tableaux de différentes tailles.

Conclure sur leur complexité.

Comparer la complexité de ces algorithmes à celle de ceux étudiés au [TD précédent](../TD7).
