---
title: TD3 - Tableaux
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - revoir la structure de donnée la plus simple : le tableau
    - implémenter des fonctions nécessitant de parcourir un tableau

## Programme de départ

On commence par la fonction la plus simple, dont le rôle est d'afficher les éléments du tableau à une dimension `tab` de taille `taille`.

``` c
void affiche(int tab[], unsigned taille)
{
    for (size_t i = 0; i < taille; i++) {
        printf("tab[%u] = %d\n", i, t[i]);
    }
}
```

On testera cette fonction dans le programme suivant, qui réalise une allocation **dynamique**.

``` c
int main(void)
{
    unsigned taille = 10;
    /* allocation sur le tas */
    int* tab = malloc(taille * sizeof(int));
    tab[0] = 2; tab[1] = 8; tab[2] = 1; tab[3] = 4; tab[4] = 10;
    tab[5] = 5; tab[6] = 6; tab[7] = 3; tab[8] = 9; tab[9] = 7;
    affiche(tab, taille);
    /* quand on n'a plus besoin de tab, il faut libérer la mémoire utilisée */
    free(tab);
    return EXIT_SUCCESS;
}
```

## Exercices

### Parcours simples

Les exercices suivants réalisent des opérations sur un tableau en le parcourant de manière simple.
Le prototype de la fonction à coder est donné pour chaque exercice.
Pensez à **tester** systématiquement la fonction une fois celle-ci codée.

#### Exercice 1 : calcul du minimum

Cette fonction retourne le minimum d'un tableau.

``` c
int min(int tab[], unsigned taille);
```

#### Exercice 2 : recherche d'un élément

Cette fonction indique si un élément est présent dans un tableau.

``` c
bool est_present(int tab[], unsigned taille, int valeur);
```

Quelle est sa complexité en fonction de la taille du tableau ?

#### Exercice 3 : recherche linéaire d'un élément dans un tableau trié

Cette fonction indique si un élément est présent dans un tableau **trié** en effectuant une recherche **linéaire** dans le tableau.

``` c
bool est_present_trie(int tab[], unsigned taille, int valeur);
```

Quelle est sa complexité en fonction de la taille du tableau ?

#### Exercice 4 : recherche dichotomique d'un élément dans un tableau trié

Cette fonction indique si un élément est présent dans un tableau **trié** en effectuant une recherche **dichotomique** dans le tableau.

``` c
bool est_present_trie_dicho(int tab[], unsigned taille, int valeur);
```

Quelle est sa complexité en fonction de la taille du tableau ?
Comparez cette complexité à celle de la fonction de [l'exercice 3](#exercice-3-recherche-lineaire-dun-element-dans-un-tableau-trie).

### Autres exercices sur les tableaux

#### Exercice 5 : un tableau est-il une permutation d'ordre N ?

La fonction suivante indique si un tableau `p` est une permutation :

``` c
bool est_permut(unsigned p[], unsigned taille);
```

!!! exemple "Exemples de permutations d'ordre 3"
    Le tableau `[2, 0, 1]` est une permutation d'ordre 3.  
    Le tableau `[0, 1, 2]` est une permutation d'ordre 3.  
    Le tableau `[1, 0, 1]` n'est pas une permutation d'ordre 3.  
    Le tableau `[3, 0, 1]` n'est pas une permutation d'ordre 3.

Deux stratégies (au moins) sont envisageables :

1. On raisonne sur la séquence des entiers de $1$ à $n$ et on recherche si elle comporte un élément qui   n'appartient pas à `p`.
2. On énumère la séquence des éléments de `p` , en s'assurant que chaque élément fait partie de l'intervalle et n'a pas encore été rencontré.

Comparer les complexités respectives, en temps et en mémoire, de ces deux solutions.

!!! info "Application"
    Les permutations sont une notion centrale en [cryptographie](https://fr.wikipedia.org/wiki/Cryptographie). Pour assurer la sécurité des algorithmes de [chiffrement](https://fr.wikipedia.org/wiki/Chiffrement) par exemple, les permutations utilisées doivent satisfaire certaines propriétés, en particulie de non-linéarité. Elles sont notamment utilisées par exemple pour le [chiffrement par transposition](https://fr.wikipedia.org/wiki/Chiffrement_par_transposition) ou dans les algorithmes de [chiffrement par bloc](https://fr.wikipedia.org/wiki/Chiffrement_par_bloc).

#### Exercice 6 : tableau 2D représentant une relation

Le but de cet exercice est de fournir des fonctions pour manipuler des [relations](https://fr.wikipedia.org/wiki/Relation_binaire).

Plus précisément, étant donné un ensemble fini de points $E=\{1,...,n\}$, on représente une relation $R$ sur $E\times E$ par une matrice carrée à valeurs dans les booléens.

!!! exemple 
    Soit un ensemble $E=\{1, 2, 3\}$, où les relations suivantes sont définies :
    
    * $1 \rightarrow 2$
    * $1 \rightarrow 3$
    * $2 \rightarrow 3$
    
    Cette relation est décrite par la matrice suivante :
    
    $$
    \begin{bmatrix}
    0 & 1 & 1\\
    0 & 0 & 1\\
    0 & 0 & 0\\
    \end{bmatrix}
    $$

Écrire les fonctions suivantes qui vérifient les propriétés d'une relation `r`.

1. Une fonction qui indique si une relation est [réflexive](https://fr.wikipedia.org/wiki/Relation_binaire#Relation_r%C3%A9flexive).
   ``` c
   bool est_reflexive(bool r[3][3]);
   ```
   La matrice suivante décrit une relation refléxive :
   
    $$
    \begin{bmatrix}
    1 & 1 & 0\\
    0 & 1 & 0\\
    1 & 0 & 1\\
    \end{bmatrix}
    $$
    
    ``` c
    bool relation_reflexive[3][3] = {{1, 1, 0}, {0, 1, 0}, {1, 0, 1}};
    ```

    La matrice suivante décrit une relation que n'est pas refléxive :
   
    $$
    \begin{bmatrix}
    1 & 0 & 0\\
    0 & 1 & 0\\
    0 & 1 & 0\\
    \end{bmatrix}
    $$

    ``` c
    bool relation_non_reflexive[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 1, 0}};
    ```

2. Une fonction qui indique si une relation est [antisymétrique](https://fr.wikipedia.org/wiki/Relation_antisym%C3%A9trique).
   ``` c
   bool est_antisymetrique(bool r[3][3]);
   ```
       La matrice suivante décrit une relation antisymétrique :
   
    $$
    \begin{bmatrix}
    1 & 1 & 0\\
    0 & 1 & 0\\
    0 & 0 & 1\\
    \end{bmatrix}
    $$
    
    ``` c
    bool relation_antisymetrique[3][3] = {{1, 1, 0}, {0, 1, 0}, {0, 0, 1}};
    ```

    La matrice suivante décrit une relation que n'est pas antisymétrique :
   
    $$
    \begin{bmatrix}
    0 & 1 & 0\\
    1 & 0 & 0\\
    0 & 0 & 0\\
    \end{bmatrix}
    $$

    ``` c
    bool relation_non_antisymetrique[3][3] = {{0, 1, 0}, {1, 0, 0}, {0, 0, 0}};
    ```

3. Une fonction qui indique si une relation est [symétrique](https://fr.wikipedia.org/wiki/Relation_sym%C3%A9trique).
   ``` c
   bool est_symetrique(bool r[3][3]);
   ```
   La matrice suivante décrit une relation symétrique :
   
    $$
    \begin{bmatrix}
    0 & 1 & 1\\
    1 & 0 & 0\\
    1 & 0 & 0\\
    \end{bmatrix}
    $$
    
    ``` c
    bool relation_symetrique[3][3] = {{0, 1, 1}, {1, 0, 0}, {1, 0, 0}};
    ```

    La matrice suivante décrit une relation que n'est pas symétrique :
   
    $$
    \begin{bmatrix}
    0 & 1 & 0\\
    1 & 0 & 0\\
    0 & 1 & 0\\
    \end{bmatrix}
    $$

    ``` c
    bool relation_non_symetrique[3][3] = {{0, 1, 0}, {1, 0, 0}, {0, 1, 0}};
    ```

4. Une fonction qui indique si une relation est [transitive](https://fr.wikipedia.org/wiki/Relation_transitive).
   ``` c
   bool est_transitive(bool r[3][3]);
   ```
   La matrice suivante décrit une relation transitive :
   
    $$
    \begin{bmatrix}
    0 & 1 & 1\\
    0 & 0 & 1\\
    0 & 0 & 0\\
    \end{bmatrix}
    $$
    
    ``` c
    bool relation_transitive[3][3] = {{0, 1, 1}, {0, 0, 1}, {0, 0, 0}};
    ```

    La matrice suivante décrit une relation que n'est pas transitive :
   
    $$
    \begin{bmatrix}
    0 & 1 & 0\\
    1 & 0 & 0\\
    1 & 0 & 0\\
    \end{bmatrix}
    $$

    ``` c
    bool relation_non_transitive[3][3] = {{0, 1, 0}, {1, 0, 0}, {1, 0, 0}};
    ```
