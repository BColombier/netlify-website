---
title: TD1 - Environnement/rappels de C
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - prendre en main les outils utilisés dans ce cours
    - coder quelques programmes simples en C
    
## Outils du développeur

### Unix

#### Commandes

Nous travaillons sous un environnement GNU/Linux.
Assurez-vous d'être à l'aise avec la ligne de commande et de maîtriser les commandes de base : `ls`, `cd`, `grep`, `man`...
Vous pouvez vous remettre à niveau au [lien suivant](https://ensiwiki.ensimag.fr/index.php?title=Stage_Unix_de_rentr%C3%A9e).

#### Raccourcis clavier

Quelques raccourcis clavier sont bien utiles lorsqu'on travaille dans le terminal :

  * `Tab` : complétion (à utiliser sans modération !)
  * `Ctrl+L` : vider le terminal
  * `Ctrl+R` : rechercher dans l'historique des commandes

### Gestion de versions avec Git

Les commandes utiles pour l'utilisation que nous ferons de Git, notamment pour le TP, sont :

  - `git init` : initialiser un dépôt Git dans le répertoire courant
  - `git status` : afficher le status du dépôt (fichiers modifiés, non suivis, avec des modifications validées, etc)
  - `git add FICHIER.c`: prendre en compte la **création** ou les **modifications** de `FICHIER.c`
  - `git commit -m "message"` : regrouper les modifications dans un *commit*
  - `git push` : pousser nos modifications locales vers la branche distante
  - `git pull` : intégrer la branche distante à notre dépôt local

!!! info "Utilisation classique"
    Lorsqu'on travaille sur un projet, on effectue régulièrement la suite d'actions suivante :
    
    - `git status`
    - `git add FICHIER.c`
    - `git status`
    - `git commit -m "message"`
    - `git push`

!!! info "Bonnes pratiques"
    - Pensez à faire des *commit* **très régulièrement** pour placer des "points d'historique" dans votre projet.
    - Choisissez un message **concis et explicite** pour chaque *commit*.

### Compilation

Après avoir **édité** le fichier source avec votre éditeur de texte favori, vous devez le **compiler** en exécutant la commande suivante :

```terminal
gcc -std=c99 -Wall -Wextra -g FICHIER_SOURCE.c -o EXECUTABLE
```
Les options passées au compilateur ont pour rôle :
    
  * `std=c99` : de compiler en suivant la [révision C99](https://en.wikipedia.org/wiki/C99) du langage C
  * `-Wall` : d'afficher des avertissements
  * `-Wextra` : d'afficher encore plus d'avertissements
  * `-g` : d'inclure des informations de [débuggage](#debogueur) dans l'exécutable

On passe en paramètre le fichier source à compiler : ici `FICHIER_SOURCE.c`, mais que l'on prendra soin de renommer de manière plus explicite quant à son contenu.

On définit ensuite le nom de l'éxecutable à générer : ici `EXECUTABLE`, à modifier également.

Une fois la compilation effectuée avec succès, vous pouvez enfin exécuter le programme avec la commande suivante :

```terminal
./EXECUTABLE
```

!!! danger "Dossier courant et `PATH`"
    Ne pas oublier le `./` devant le nom de l'exécutable.
    En effet, il faut lancer l'exécutable qui se trouve dans le **dossier courant** (`./`), et non pas un programme accessible via la variable d'environnement [`PATH`](https://fr.wikipedia.org/wiki/Variable_d%27environnement#%3CPATH%3E_pour_l'emplacement_des_ex%C3%A9cutables).

!!! warning "Avertissements (ou *warnings*)"
    Si des *warnings* apparaîssent à la compilation, il faut les corriger.
    En effet, même si le programme est correct d'un point de vue syntaxique, ces *warnings* indiquent qu'un problème potentiel est tapis dans l'ombre et prêt à surgir.

### Répertoire de travail

On organisera notre répertoire de travail de la manière suivante :

  * Un fichier [Makefile](#makefile)
  * Un dossier `src` contenant les fichiers sources (`*.c`)
  * Un dossier `include` contenant les fichiers d'en-tête (`*.h`)
  * Un dossier `bin` contenant les exécutables générés.

### Makefile

Pour simplifier l'étape de compilation, on utilise un Makefile.
Le Makefile de base à utiliser se trouve au [lien suivant](../assets/Makefile).

### Débogueur

Le débogueur (ou *debugger*) permet de trouver plus facilement des *bugs* dans nos programmes.
Parmis ses nombreuses fonctionnalités, nous utiliserons principalement l'exécution pas-à-pas et l'affichage du contenu des variables.

!!! warning "Attention"
    Vous **devez** vous familiariser avec ces outils, qu'il est indispensable de savoir utiliser.
    En effet, déboguer un programme avec des `printf` judicieusement placés ne suffit plus lorsque la complexité augmente.

#### GDB

On utilisera le débogueur GDB (GNU Debugger).

!!! warning "Option de compilation" 
    Pour utiliser le débogueur sur votre exécutable, il est indispensable de l'avoir compilé avec l'option `-g`.  
    Pour plus de détails : `man gcc | grep '\-g'`

Pour lancer le débogueur sur l'exécutable, exécuter la commande suivante :

``` terminal
gdb ./EXECUTABLE
```

L'interface en ligne de commande de GDB s'affiche, qui devrait ressembler à :

``` terminal
GNU gdb (Ubuntu 9.2-0ubuntu1~20.04) 9.2
Copyright (C) 2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from EXECUTABLE...
(gdb) 
```

!!! warning "*debugging symbols*"
    Si la ligne `(No debugging symbols found in EXECUTABLE)` s'affiche, vous avez oublié l'option `-g` à la compilation.
    
Les commandes suivantes peuvent ensuite être utilisées :

  * `break` (ou `b`) : placer un point d'arrêt (ou *breakpoint*)
    * `b 14` : placer un point d'arrêt à la ligne 14
    * `b addition.c:18` : placer un point d'arrêt à la ligne 18 du fichier `addition.c`
    * `b multiplication` : placer un point d'arrêt au début de la fonction `multiplication`
  * `watch` : observer les changements de valeur d'une variable. Attention, il faut que la variable soit connue, donc qu'on soit dans le contexte où elle est définie. On peut se palcer dans ce contexte en y plaçnt un point d'arrêt.
  * `run` (ou `r`) : exécuter le programme
  * `continue` (ou `c`) : continuer l'exécution après qu'elle ait été suspendue, à un point d'arrêt par exemple.
  * `quit` (ou `q`) : quitter

!!! info "Utilisation classique"
    Supposons qu'on veuille observer les valeurs successives de la variable `octet` dans une boucle *for*.
    La variable `octet` est modifiée dans le corps de la boucle *for*, qui commence à la ligne 53 du fichier `operations.c`.
    
    On réalisera la suite d'opérations suivante :
    
    * `b operations.c:53`
    * `r`
    * `watch octet`
    * `c` (autant de fois que nécessaire pour observer les valeurs successives)
    * `q`

#### Valgrind

Valgrind est un débogueur mémoire qui nous permettra, en particulier, de s'assurer que nous gérons la mémoire correctement lorsque nous réalisons des allocations dynamiques.
Il possède des nombreuses autres fonctionnalités.
Une introduction à Valgrind est disponible sur Ensiwiki au [lien suivant](https://ensiwiki.ensimag.fr/index.php?title=Valgrind).

Voici un exemple de ce qu'affiche Valgrind lorsque la mémoire n'est pas libérée :

``` terminal hl_lines="3 14"
==13409== HEAP SUMMARY:
==13409==     in use at exit: 304 bytes in 12 blocks
==13409==   total heap usage: 15 allocs, 3 frees, 1,348 bytes allocated
==13409== 
==13409== LEAK SUMMARY:
==13409==    definitely lost: 116 bytes in 7 blocks
==13409==    indirectly lost: 188 bytes in 5 blocks
==13409==      possibly lost: 0 bytes in 0 blocks
==13409==    still reachable: 0 bytes in 0 blocks
==13409==         suppressed: 0 bytes in 0 blocks
==13409== Rerun with --leak-check=full to see details of leaked memory
==13409== 
==13409== For lists of detected and suppressed errors, rerun with: -s
==13409== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

!!! danger "Erreurs"
    Malgré le fait que la dernière ligne indique `0 errors`, on voit sur la troisième ligne que le programme réalise 15 allocations, mais seulement 3 libérations.
    Il faudra corriger ça !

### *Profiling*

**Profiler** un programme consiste à obtenir des métriques de performance sur ce dernier.
On s'intéressera ici à la mesure de la durée d'exécution totale.

#### Commande `time`

On préfixe la commande d'exécution du programme avec `time`.
Par exemple, pour profiler le programme `test_hash_table 500000`, on lancera la commande suivante :

``` terminal
time ./bin/test_hash_table 500000
```

On obtient l'information qui nous intéresse : notre programme s'est exécuté en 0.283 secondes.

``` terminal
./bin/test_hash_table 500000  0,26s user 0,02s system 99% cpu 0,283 total
```

#### Utilitaire `gprof`

`gprof` fait partie des utilitaires `GNU Binutils`.
Contrairement à `time`, il permet d'avoir une granularité plus fine dans l'analyse du temps d'exécution, en descendant au niveau des fonctions.

Pour utiliser `gprof` sur un exécutable, il faut :

  1. compiler avec l'option `-pg`
  2. exécuter l'exécutable une première fois avec ses arguments éventuels
  3. exécuter `gprof -b -Q ./EXECUTABLE` sans les arguments de l'exécutable.
  
Voici un exemple de sortie obtenue :

``` terminal  hl_lines="6 7 8"
Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
 63.63      0.12     0.12   999999     0.00     0.00  fusion
 31.82      0.18     0.06        1    60.45   181.35  tri_fusion
  5.30      0.19     0.01        1    10.07    10.07  generate_data
```

On en déduit que :

  * la fonction `fusion`a été appelée 999999 fois pour une durée totale de 0.12 secondes.
    Chaque appel a pris moins de 0.01 ms.
  * la fonction `tri_fusion`a été appelée 1 fois pour une durée totale de 0.06 secondes.
    La durée cumulée est alors de 0.18 (0.12 + 0.06) secondes.
    L'appel à cette fonction seule a pris 60.45 ms, tandis que le total incluant les sous-appels a pris 181.35 ms.
  * la fonction `tri_fusion`a été appelée 1 fois pour une durée totale de 0.01 secondes.
    La durée cumulée est alors de 0.19 (0.18 + 0.01) secondes.
    Il n'y a pas eu de sous-appels car la durée totale est égale à la durée de la fonction seule.
    L'appel a pris 10.07 ms.

La durée d'exécution totale est celle cumulée donnée sur la dernière ligne, soit 0.19 secondes ici.
    
## Exercices

!!! info "Information"
    Ces exercices peuvent sembler s'adresser aux débutants en C. Néanmoins, il est important que tout le monde s'assure de les **maîtriser** avant de passer au TD suivant. Si c'est déjà le cas pour vous, vous pouvez passer directement au [TD2](TD2).
    
!!! warning "Validation d'un exercice"
    Pour considérer que vous avez terminé un exercice, il faut :
    
    * Que la compilation se fasse sans *warning*
    * Que toute la mémoire que vous avez allouée est bien libérée (à vérifier avec [Valgrind](#valgrind))
    * Que vous ayez testé le **bon fonctionnement** de votre fonction dans un programme principal. Il est possible de ne créer qu'un seul fichier pour tous les exercices de ce TD, avec un seul programme principal qui testera toutes les fonctions.

!!! tip "Astuce : le compilateur est votre ami"
    Compilez **aussi souvent que possible**.
    Le compilateur est très rapide et bien meilleur que nous tous réunis pour trouver les erreurs dans votre programe : n'attendez pas que ces dernières s'empilent pour les corriger !

### Variables et affectations

#### Exercice 1 : échange des valeurs de 2 variables

``` c
void echange() {
    int t, x, y;

    printf("Entrez l'entier x\n");
    scanf("%d", &x); /* ici x vaut x0 */
    printf("Entrez l'entier y\n");
    scanf("%d", &y); /* ici y vaut y0 */
    x = y;           /* point d'observation (1) : x vaut ..., y vaut ... */
    t = x;           /* point d'observation (2) : t vaut ... */
    y = t;           /* point d'observation (3) : y vaut ... */
    printf("x = %d ; y = %d\n", x, y);
}

int main(void) {
    echange();
    return EXIT_SUCCESS;
}
```

  1. Écrire des assertions sur l'état du programme aux points d'observation (1), (2) et (3). En déduire ce qui sera affiché à la fin.
  2. En modifiant l'ordre des affectations, obtenir un programme dont l'effet est d'**échanger** les valeurs des deux variables avant l'affichage final.
  3. Généralisation : écrire un programme qui réalise une permutation circulaire des valeurs de 3 variables, puis 4.

#### Exercice 2 : préparation aux itérations

  1. Écrire un programme qui lit 5 entiers et en affiche la somme.
  2. En donner une version en utilisant le moins de variables possible et en essayant de répéter 5 fois les mêmes opérations.
  3. Même question pour lire les entiers `x1` à `x6` et afficher la valeur de `x1*x2 + x3*x4 + x5*x6`.

### Fonction, passage de paramètres

#### Exercice 3 : fonction d'échange

Écrire une fonction qui prend 2 entiers en paramètres et qui les échange.

``` c
void echange(int* x, int* y);
```

Attention, les paramètres sont passés par **adresse**.
Vérifiez que l'échange est correct en testant la fonction dans un programme principal.

#### Exercice 4 : fonctions et effet des paramètres

Identifiez ce que font les fonctions suivantes :
  
``` C
void mystere (int a, int* b, int* c) {
    /*  ici a vaut a0, b vaut b0, c vaut c0 */

    (*c)++;      /* ici ... */
    *b = *c + a; /* ici ... */
}

void que_fais_je() {
    int x, y, z;

    scanf("%d", &x);
    scanf("%d", &z);
    mystere(x + 1, &y, &z);
    printf("x = %d ; y = %d ; z = %d\n", x, y, z);
}
```

#### Exercice 5 : appel de fonctions

``` C
int f(int x) {
    /* Si X vaut x0 alors le retour vaut x+1 */
    return x + 1;
}

int g(int a) {
    /* Si a vaut a0 alors le retour vaut ... */
    int x = a + 1;
    return x * x;
}

void que_fais_je2() {
    int x;

    scanf("%d", &x); /* si on entre 4 ici */
    x = x + f(x - 1);
    x = x + f(g(x + 1));
    printf("x = %d\n", x); /* alors là, ça affiche ? */
}
```

Donner la formule générale, en fonction de la valeur de `x` fournie par l'utilisateur.

#### Exercice 6 : réflexion sur les effets de bord

``` C
unsigned compteur_appels;

int effet(int a) {
    printf(" coucou ! \n"); /* ligne 3 */
    compteur_appels = compteur_appels + 1;
    return a + compteur_appels;
}

void effet_bord() {
    int x, y;

    compteur_appels = 0;
    scanf("%d", &x);
    y = effet(x) + effet(x);
    printf("%d\n", y); /* ligne 1 */
    y = 2 * effet(x);
    printf("%d\n", y); /* ligne 2 */
    printf("ca = %u\n", compteur_appels);
}
```

  1. Quel est le comportement de ce programme ? Que voit-on s'afficher ? Est-ce que les deux lignes 1 et 2 ont le même effet ?
  2. Que pensez-vous de l'action de la première ligne de la fonction `effet` ?

!!! danger "Effets de bord (*side-effect*)"
    Une fonction à effets de bord **modifie l'état global** du programme.
    Par exemple : modification d'un paramètre, d'une variable externe à la fonction, interaction avec les entrées / sorties (par exemple, clavier, écran), levée d'exception…

### Expressions booléennes et structure de contrôle conditionnelle

#### Exercice 7 : opérateurs booléens

``` C
void expr_bool() {
    bool a = false;
    bool b = false;
    bool c = false;
    bool d = false;

    d = (a && b) || c;
    a = false;
    b = !false;
    c = (a && !b) || c;
    d = !c;
    d = !d  || a;
    if (d) {
        a = false;
    } else {
        a = !false;
    }
    a = !d;
    a = d == false;
}
```

  1. Indiquer les valeurs des différentes variables booléennes après chaque affectation.
  2. Comparer les trois dernières lignes. Donner une expression équivalente à `a == true` mais plus simple.

#### Exercice 8 : une suite d'entiers est-elle croissante ?

Dans le même esprit que [l'exercice 2](#exercice-2-preparation-aux-iterations), écrire un programme qui lit 6 entiers et affiche "oui" une fois la saisie terminée si ces entiers forment une suite strictement croissante et "non" sinon.

Difficulté supplémentaire : ne pas utiliser de structures conditionnelles sauf au moment de l'affichage.

#### Exercice 9 : signe du produit

Écrire une fonction à deux paramètres entiers et à résultat booléen, vrai si le signe du produit est strictement positif, faux sinon.
Ne pas utiliser ni structures conditionnelles, ni l'opérateur de multiplication. 

#### Exercice 10 : construire les opérateurs booléens

Sans utiliser les opérateurs booléens, mais uniquement la construction `if then else` et les constantes `true` et `false`, écrire des fonctions à paramètres et résultat de type booléen, qui réalisent les opérations suivantes :

  * [NON](https://fr.wikipedia.org/wiki/N%C3%A9gation_logique)
  * [ET](https://fr.wikipedia.org/wiki/Conjonction_logique)
  * [OU](https://fr.wikipedia.org/wiki/Disjonction_logique)
  * [OU-exclusif](https://fr.wikipedia.org/wiki/Fonction_OU_exclusif)
  * [implication](https://fr.wikipedia.org/wiki/Implication_(logique))
  * [équivalence](https://fr.wikipedia.org/wiki/%C3%89quivalence_logique)
  
On s'autorise aussi à réutiliser les fonctions précédemment écrites. 

#### Exercice 11 : lois de De Morgan

Comparer les trois blocs de code suivants :

``` c
if (a && b) {
    action1;
} else {
    action2;
}

if (!(a && b)) {
    action2;
} else {
    action1;
}

if (!a || !b) {
    action2;
} else {
    action1;
}
```

En déduire les (deux) lois de De Morgan.

#### Exercice 12 : opérateurs paresseux

Discuter les deux lignes suivantes (`x` et `y` sont de type `int`) :

``` c
x != 0 && y / x == 1;
y / x == 1 && x != 0;
```
Comparer

``` c
if (a && b) action1; else action2;
```
et

``` c
if (a) {
    if (b) {
        action1;
    } else {
        action2;
    }
} else {
    action2;
}
```
Donner une action conditionnelle n'utilisant que des conditions simples et qui est équivalente à :

``` c
if (a || b) action1; else action2;
```
Donner une action conditionnelle équivalente à : 

``` c
if (e1 && (e2 || e3)) action1; else action2;
```

#### Exercices 13 : transformations de programmes avec des conditionnelles

On considère les deux lignes suivantes :

``` c
if (e) action1; else action2;
```
``` c
if (e) action1; if (!e) action2;
```

  1. Trouver une condition `e` et des actions `action1` et `action2` telles que ces deux lignes soient différentes.
  2. Préciser votre interprétation de "différentes" dans ce contexte.

Donner une autre forme de :

``` c
if (e) {action1; action3;}
else {action2; action3;}
```

#### Exercice 14 : calcul du maximum

Écrire une fonction qui calcule le maximum de deux entiers et qui a pour prototype :
``` c
int max2(int x, int y);
```
Sur le même modèle, écrivez les fonctions `max3` et `max4` qui opèrent sur 3 et 4 variables respectivement.

Comparez les différentes solutions en comptant le nombre d'opérations de comparaison dans le pire cas et dans le meilleur cas.
