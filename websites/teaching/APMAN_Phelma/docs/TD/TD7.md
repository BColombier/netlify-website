---
title: TD7 - Algorithmes de tris simples
---

!!! check "Objectifs"
    Les objectifs de ce TD sont :
    
    - implémenter des algorithmes de tri simples
    - comparer deux algorithmes réalisant la même fonction

### Vérification du bon fonctionnement

#### Génération de données de test

Vous pouvez utiliser le code suivant pour générer un tableau de $n$ entiers aléatoires.

``` c
#include <time.h>

int* generer_donnees_test(unsigned n)
{
  int* donnees_test = malloc(n * sizeof(int));
  srand(time(NULL));
  for (size_t i = 0; i < n; i++) {
    donnees_test[i] = rand() % (10 * n);
  }
  return donnees_test;
}
```

!!! Warning "Libération"
    Pensez à libérer ce tableau à la fin de votre programme !
    
#### Vérification de correction

Vous pouvez utiliser des assertions pour vérifier que le tableau a bien été trié.

``` c
#include <assert.h>

assert(1 < 2);
```

### Exercices

#### Exercice 1 : tri par maximum

Implémenter l'algorithme de tri par maximum (également appelé tri par sélection).
Il fonctionne de la manière suivante :

- Chercher le **plus grand** élément du tableau
- Le placer à la **fin** du tableau
- Recommencer en considérant le tableau privé de son dernier élément

``` c
void tri_max(int tab[], unsigned taille);
```

!!! note "Tri par minimum"
    On peut procéder de manière similaire en cherchant le **plus petit** du tableau et en le plaçant au **début**.

#### Exercice 2 : tri par insertion

Implémenter l'algorithme de tri par insertion.
Il fonctionne de la manière suivante :

- Remarquer que **le premier** élément du tableau constitue un tableau **trié**
- Placer le **second** élément du tableau au **bon endroit** dans ce tableau trié
- Remarquer que les deux premiers éléments du tableau constituent un tableau trié
- Placer le troisième élément du tableau au bon endroit dans ce tableau trié
- Recommencer jusqu'à considérer le dernier élément du tableau

``` c
void tri_insertion(int tab[], unsigned taille);
```

!!! note "Tri d'un jeu de cartes"
    Pour la plupart des personnes, le tri par insertion est le tri utilisé "naturellement" pour trier à la main un jeu de cartes.

#### Exercice 3 : comparaison

Comparer les temps d'exécutions des deux algorithmes précédents pour des tableaux de différentes tailles.

Conclure sur leur complexité.
