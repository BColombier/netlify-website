---
hide:
  - navigation
---

<!-- !!!danger -->
<!--     Le contenu de cette page n'est pas figé et est susceptible d'être modifié jusqu'au lancement du TP, qui aura lieu lors de la 6ème séance de TD. -->

# TP : vérificateur orthographique

!!! warning "Inscription"
    Inscrivez votre binôme dans le fichier suivant : [lien](https://cloud.minatec.grenoble-inp.fr/index.php/s/Czxd8o9bFSGdFwG).  
    Cliquez sur "**Ouvrir dans ONLYOFFICE**" pour modifier le fichier.
## Objectif

L'objectif de ce TP est de programmer un vérificateur orthographique en C qui, après avoir construit un dictionnaire à partir d'un fichier donné, parcourra un texte et **affichera** les mots **n'appartenant pas** au dictionnaire.

Votre programme devra prendre deux paramètres, **passés à l'exécutable en ligne de commande** :

- le nom du fichier de dictionnaire (par exemple `FR.txt`),
- le nom du fichier texte à vérifier (par exemple `a_la_recherche_du_temps_perdu.txt`).

La commande à exécuter sera alors :

``` bash
./verif_ortho FR.txt a_la_recherche_du_temps_perdu.txt
```

Les deux fichiers à utiliser pour ce projet sont disponibles aux liens suivants :

- [:fontawesome-regular-file-alt: FR.txt](./assets/FR.txt)
- [:fontawesome-regular-file-alt: a_la_recherche_du_temps_perdu.txt](./assets/a_la_recherche_du_temps_perdu.txt)

## Consignes

Vous devrez donc implémenter le dictionnaire et le vérificateur de différentes manières (**au moins trois**) et les **comparer**.
Vous pourrez par exemple utiliser **structures de données** (pensez à celles vues en cours, et celles listées plus bas dans la section [Aide](#aide)) ou différentes **méthodes** pour exploiter ces structures de données.

L'accent est mis sur la **prise de recul** vis à vis de vos implémentations, et comment vous les **évaluerez** dans le rapport.
Vous devrez donc avoir des implémentations qui **fonctionnent**, mais ce n'est en aucun cas **suffisant** pour avoir la moyenne.
Il faut que le rapport montre que vous avez fait des **choix** lors des implémentations, et que vous **explicitiez** ces choix.
Soyez **critiques**, et prenez en compte autant d'aspects que nécessaire pour comparer vos implémentations.


## Organisation

Vous travaillerez **en binôme** sur le projet et collaborerez en **utilisant Git**.

!!! warning
    Assurez-vous d'avoir configuré Git en indiquant vos noms, prénoms et adresse e-mail sur la machine avec laquelle vous travaillez.  
    Par exemple, si vous vous appelez Alice Dupont, exécutez :
    ``` bash
    $ git config --global user.name "Alice Dupont"
    $ git config --global user.email alice.dupont@phelma.grenoble-inp.fr
    ```
    
Lorsque vous réalisez un *commit*, utilisez un message **explicite** pour décrire ce que vous venez d'implémenter.
Votre dépôt Git fera partie du rendu final, donc maintenez-le correctement.
En particulier, assurez-vous que le dépôt Git fait bien apparaître **les contributions des deux membres** du binôme.

## Rendu

Votre rendu sera un dépôt Git, auquel vous nous donnerez les droits **Owner**, et regroupant :

- les **sources** de votre programme,
- un README succinct expliquant :
    - **comment compiler** le projet,
    - **comment utiliser** les exécutables générés.
- un rapport qui détaillera **vos choix** d'implémentation et présentera une **comparaison** des différentes solutions que vous proposerez. La forme du rapport, en particulier son nombre de pages, est **libre**. Il devra être rendu au **format PDF**.

Un rendu typique sera organisé de la manière suivante :

```
├── README
├── rapport.pdf
├── Makefile
├── include
│   ├── dico_toto.h
│   ├── dico_tata.h
│   ├── dico_titi.h
│   └── utilitaires.h
├── src
│   ├── dico_toto.c
│   ├── dico_tata.c
│   ├── dico_titi.c
│   └── utilitaires.c
└── test
    ├── test_dico_toto.c
    ├── test_dico_tata.c
    └── test_dico_titi.c
```

## Critères d'évaluation

- Compilation sans erreurs ni *warnings*
- Absence de fuites mémoire (Valgrind est votre ami)
- Fonctionnement correct
    - Tout mot faux est bien identifié comme faux
    - Tout mot correct est bien identifié comme correct
- Code propre et lisible
    - Indentation cohérente
    - Variables nommées explicitement
    - Commentaires succincts
    - Découpage fonctionnel judicieux
- Implémentation
    - **Au moins trois** implémentations proposées, dont une recherche séquentielle
    - Structures de données claires
    - Opérations bien définies et de complexité connue
    - Discussion sur la complexité et intérêt des structures utilisées
- Comparaison détaillée des implémentations
    - chiffres précis pour la **rapidité** et l'**empreinte mémoire**
    - conclusions quant à l'implémentation à privilégier pour quel usage

### Exemples de rendus et notes associées

#### Rendu non-fonctionnel : 0/20

- Le projet ne compile pas.

OU

- Une seule implémentation qui ne fonctionne pas. 

#### Rendu faible : 6 - 9/20

- Trois implémentations utilisant des structures de données simples qui fonctionnent correctement.
- Rapport décrivant le code, sans prise de recul.
- Choix techniques inexpliqués.
- Code non-structuré et peu lisible.

#### Rendu moyen : 10 - 13/20

- Trois implémentations utilisant des structures de données simples qui fonctionnent correctement.
- Rapport comparant bien les structures implémentées.
- Choix techniques mentionnés.
- Code structuré et commenté clairement.

#### Rendu bon : 14 - 17/20

- Trois implémentations utilisant des structures de données, dont une arborescente, qui fonctionnent correctement.
- Rapport comparant bien les structures implémentées avec des données chiffrées.
- Choix techniques détaillés.
- Code très bien organisé et lisible.

#### Rendu excellent : 18 - 20/20

- Trois implémentations (ou plus) de structures de données, dont une arborescente et optimisée, qui fonctionnent correctement.
- Rapport comparant bien les structures implémentées avec des données chiffrées et des graphes.
- Choix techniques détaillés et justifiés.
- Code très bien organisé et lisible.

## Aide

Vous pouvez vous inscrire sur le salon Riot  
(à rejoindre via le lien suivant : [https://matrix.to/#/!oKaJoMqJbKeddMfrWp:ensimag.fr?via=ensimag.fr](https://matrix.to/#/!oKaJoMqJbKeddMfrWp:ensimag.fr?via=ensimag.fr))  
et y poser vos questions relatives au TP.

!!!danger "Connexion"
    Lors de la connexion, cliquez sur Sign In, puis spécifiez le serveur **matrix.ensimag.fr**, et connectez-vous avec vos identifiants agalan.

### Structures de données

Les deux structures de données suivantes, non vues spécifiquement en cours, mais qui sont des arbres, sont particulièrement adaptées pour stocker un dictionnaire :

- [l'arbre préfixe](https://fr.wikipedia.org/wiki/Trie_(informatique)),
- [l'arbre radix](https://fr.wikipedia.org/wiki/Arbre_radix),
- [l'arbre radix compressé par partage des suffixes](./assets/partage_suffixes.pdf).

Vous pouvez visiter les versions anglaises de ces pages pour plus de détails.

### Fonctions utiles pour manipuler du texte

Les fonctions suivantes vous seront utiles pour manipuler du texte (des chaînes de caractères) :

- [fgets](https://www.cplusplus.com/reference/cstdio/fgets/) : lire une ligne d'un fichier,
- [strcspn](https://www.cplusplus.com/reference/cstring/strcspn/) : trouver l'indice de la première occurrence d'une chaîne de caractères dans une autre chaîne de caractères,
- [strdup](https://www.cplusplus.com/reference/cstring/strdup/) : copier une chaîne de caractères, en réalisant l'allocation mémoire
- [strcasecmp](https://www.cplusplus.com/reference/cstring/strcasecmp/) : comparer deux chaînes de caractères en ignorant la casse (majuscules/minuscules),
- [strtok](https://www.cplusplus.com/reference/cstring/strtok/) : découper une chaîne de caractères selon un ou plusieurs délimiteurs.
