---
hide:
  - navigation
  - toc
---
# Organisation

## Avancement

Indiquez dans [ce document](https://edition-collaborative.grenet.fr/Products/Files/DocEditor.aspx?fileid=37441&doc=ZzV5NHlaQS9hVzZvTHBiTGxLcENtVWtBMGs3SjNubzRoOTdBek1KL2FUND0_IjM3NDQxIg2), si vous le souhaitez, le numéro du dernier exercice que vous avez eu le temps de faire pendant chaque séance de TD.
Cela nous servira à mieux dimensionner ces séances à l'avenir.
**Ces informations sont purement indicatives, et ne seront en aucun cas prises en compte dans la notation**.

## Équipe pédagogique

  - Brice Colombier ([e-mail](mailto:brice.colombier@grenoble-inp.fr)) : responsable du cours
  - Michel Desvignes ([e-mail](mailto:michel.desvignes@phelma.grenoble-inp.fr))

## Travail étudiant demandé

Ce cours a une valeur de 3 ECTS.
Puisque 1 ECTS correspond à [25 à 30h de travail étudiant](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000028543525/#JORFARTI000028543546), vous devrez fournir **au total** environ 80h de travail.

Ces 80h de travail peuvent être réparties de la manière suivante :

  - 16$\times$2h = 32h de présence en TD
  - 6h de révision et 2h de présence à l'examen final
  - 24h de travail personnel sur le projet (soutenance incluse)

Il vous reste ainsi 16h de travail personnel à effectuer, soit 1h par séance de TD.
Il est donc **tout à fait normal** de ne pas terminer complètement les exercices de TD lors de la séance, et de devoir y **travailler à nouveau en autonomie**.

!!!attention
    Ces durées ne sont qu'indicatives. En particulier, il est possible que les premiers TD vous semblent faciles et ne nécessitent pas de travail supplémentaire en autonomie. Profitez-en pour reporter les heures de travail personnel associées sur les derniers TD !

## Évaluation

Les modalités d'évaluation sont disponibles sur [Refens](https://refens.grenoble-inp.fr/Phelma/2021/modifie?id=220200000184199&type=models.Matiere).

## Ressources

### Bibliographie

[Algorithmique. Cormen, Leiserson, Rivest & Stein. Dunod](https://www.dunod.com/sciences-techniques/algorithmique-cours-avec-957-exercices-et-158-problemes)

[Algorithms. Erickson](https://jeffe.cs.illinois.edu/teaching/algorithms/book/Algorithms-JeffE.pdf) (version électronique gratuite)

### Liens

[Big-O Cheat Sheet](https://www.bigocheatsheet.com/)
