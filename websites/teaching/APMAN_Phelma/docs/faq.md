---
hide:
  - navigation
---

Cette foire aux questions a vocation à être enrichie au fur et à mesure des séances, pour constituer une base de connaissances collective.
**Contribuez-y !**

## Outils

??? "Peut-on compiler avec `clang` à la place de `gcc` ?"
    Oui, aucune différence pour notre usage.
    
??? "Faut-il systématiquement utiliser `gdb` pour débugger ?"
    Non, à vous d'utiliser la méthode la plus adaptée à votre problème. Dans certains cas, des `printf` judicieusement placés sont amplement suffisants. Dans d'autres, par exemple lors d'une *segmentation fault*, `gdb` est bien utile pour identifier la ligne fautive.
    
??? "Lorsque je compile avec le Makefile fourni, j'ai cette erreur "/opt/miniconda3/bin/../x86_64-conda-linux-gnu/sysroot/usr/lib/../lib/gcrt1.o: relocation R_X86_64_32S against symbol `__libc_csu_fini' can not be used when making a PIE object; recompile with -fPIE "
    Cette erreur est dûe à un conflit de versions de `gcc` dans la machine virtuelle Phelma. Enlevez `-pg` des options de compilation, puis relancez la commande `make`
    
## Erreurs courantes

## Astuces
