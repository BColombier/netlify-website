---
hide:
  - navigation
  - toc
---

!!!danger "Archive"
    Cette page n'est actuellement **plus utilisée**, et n'est conservée que pour des raisons d'archivage.

# Bienvenue

Page web du cours "Algorithmique et Programmation : Mise à Niveau" (ou APMAN).

Le contenu de cette page est en grande partie tiré du cours assuré jusqu'en 2020 par [Karine Altisen](https://www-verimag.imag.fr/Karine-Altisen,102.html?lang=en).
