---
title: FAQ
hide:
  - navigation
  - toc
---
# Foire aux questions

!!!question "Peut-on faire en sorte que Git ignore certains fichiers ?"
    Oui, en créant un fichier `.gitignore` dans le répertoire de votre projet.  
    Vous pouvez ignorer des fichiers spécifiques (`exemple.c`), des fichiers ayant une certaine extension (`*.o`) ou encore des répertoires complets (`bin/`).

!!!question "Je n'arrive pas à me connecter au salon Riot avec mon identifiant agalan"
    Après avoir cliqué sur le [lien suivant](https://matrix.to/#/#projet_prog_22:ensimag.fr?via=ensimag.fr), attention à bien choisir comme **serveur d'accueil** `matrix.ensimag.fr`  

### make et Makefile

!!!question "Lorsque j'exécute `make`, j'ai l'erreur `Aucun fichier ou dossier de ce type`"
    Le problème vient du fait que les répertoires `obj`et `bin` n'existent pas. Deux solutions :

      * créer les répertoires manuellement avec la commande `mkdir`,
      * exécuter `make clean`, qui supprime les répertoires `obj` et `bin` s'ils existent, puis recrée ces deux répertoires vides.

!!!question "J'en ai assez d'ajouter manuellement les exécutables à générer dans mon Makefile. Est-ce qu'on peut automatiser ça aussi ?"
    Oui ! On ajoute la ligne suivante au Makefile, afin de générer la liste des exécutables :
    ```Makefile
    EXE=$(patsubst test/%.c, bin/%, $(wildcard test/*.c))`
    ```
    Cette ligne :
      
      - identifie chaque fichier `.c` dans le répertoire `test`
      - remplace `test/%.c` par `bin/%` dans le nom de chaque fichier
      
    On obtient ainsi la liste des exécutables à générer, à ajouter comme dépendance de la directive `all` :
    ```Makefile
    all: $(EXE)
    ```

!!!question "J'en ai assez d'écrire des directives spécifiques pour les exécutables nécessitant plusieurs fichiers objets. Est-ce qu'on peut automatiser ça aussi ?"
    Oui ! On peut spécifier que chaque exécutable dépend de **tous** les fichiers objets du projet. Pour ça, on génère la liste des fichiers objets du projet :
    ```Makefile
    ALL_OBJ=$(patsubst src/%.c, obj/%.o, $(wildcard src/*.c))
    ```
    Puis on modifie la règle générique pour les exécutables :
    ```Makefile
    # Règle générique pour générer les exécutables de test (.o -> executable)
    bin/test_%: $(ALL_OBJ) obj/test_%.o
	    gcc $^ $(LDLIBS) -o $@
    ```
    L'inconvénient intrinsèque de cette méthode est que, puisque chaque exécutable dépend de **tous** les fichiers objets du projet, la modification d'un seul fichier source entraînera la re-compilation de tous les exécutables, même ceux qui ne l'utiliseraient pas si on avait pris le temps d'écrire des règles spécifiques.

!!!question "Ok merci, et le Makefile complet est donc ?"
    ```Makefile
    # Options de compilation
    # (infos debug, tous warnings, standard C99, dossier include)
    CFLAGS=-g -Wall -Wextra -std=c99 -Iinclude

    # Options d'édition des liens (pour utiliser math.h)
    LDLIBS=-lm

    # Identification des exécutables à générer à partir des fichiers de test
    EXE=$(patsubst test/%.c, bin/%, $(wildcard test/*.c))

    # Identification de tous les fichiers objets associés aux fichiers sources
    ALL_OBJ=$(patsubst src/%.c, obj/%.o, $(wildcard src/*.c))

    # Règle par défaut : générer tous les exécutables
    all: $(EXE)

    # Règle générique pour générer les fichiers objets pour les sources (.c -> .o)
    obj/%.o: src/%.c
	    gcc $(CFLAGS) -c $^ -o $@

    # Règle générique pour générer les fichiers objets pour les tests (.c -> .o)
    obj/test_%.o: test/test_%.c
	    gcc $(CFLAGS) -c $^ -o $@

    # Règle générique pour générer les exécutables de test (.o -> executable)
    bin/test_%: $(ALL_OBJ) obj/test_%.o
	    gcc $^ $(LDLIBS) -o $@

    # Règle de nettoyage
    clean:
	    rm -rf bin obj
	    mkdir bin obj
    ```
