---
hide:
  - navigation
  - toc
---

!!!danger "Archive"
    Cette page n'est actuellement **plus utilisée**, et n'est conservée que pour des raisons d'archivage.

# Bienvenue

Page web du cours "Projet programmation".

## Diapo d'introduction au projet

À récupérer ici : [:fontawesome-regular-file-powerpoint:](assets/intro_cours.pdf).

## Sujet PDF

À récupérer ici : [:fontawesome-regular-file-pdf:](assets/sujet.pdf).

## Soutenances : horaires de passage

### G1 : 12 mai

### G2 : 13 mai

### G3 : 9 mai

## Informations

!!!warning "Soutenances finales"
    - Groupe 1 : 12 mai
    - Groupe 2 : 13 mai
    - Groupe 3 : 9 mai
    
!!!info "Configuration initiale N corps"
    Ajout de la section 4.4.1 qui explique comment obtenir une configuration initiale qui fonctionne avec un grand nombre de corps, disposés autour du trou noir dans une couronne.

!!!danger "Erratum sujet 7 avril"
    - **Sujet comptage de cellules** : l'algorithme de recherche en profondeur pour le comptage des composantes connexes était incorrect.  
    Version incorrecte :
    ```
    fonction DFS(image, x, y)
    ...
    DFS(image, y-1, x)
    DFS(image, y+1, x)
    DFS(image, y, x-1)
    DFS(image, y, x+1)
    ```
    Version corrigée :
    ```
    fonction DFS(image, ligne, colonne)
    ...
    DFS(image, ligne-1, colonne)
    DFS(image, ligne+1, colonne)
    DFS(image, ligne, colonne-1)
    DFS(image, ligne, colonne+1)
    ```
    Notez l'inversion des paramètres `x` et `y` entre la définition de la fonction et les appels récursifs. D'où l'intérêt d'utiliser des noms **explicites** pour les paramètres des fonctions également !

!!!danger "Erratum sujet 6 avril"
    - **Sujet comptage de cellules** : l'algorithme de reconstruction morphologique était incorrect.  
    Version incorrecte :
    ```
    faire
        graine_dilatee = dilatation(graine)
        image_reconstruite = intersection(image, graine_dilatee)
    tant que image_reconstruite change
    ```
    Version corrigée :
    ```
    graine_dilatee = dilatation(graine)
    image_reconstruite = intersection(image, graine_dilatee)
    faire
        graine_dilatee = dilatation(image_reconstruite)
        image_reconstruite = intersection(image, graine_dilatee)
    tant que image_reconstruite change
    ```
    C'est bien l'image reconstruite que l'on dilate à chaque itération, pas la graine initiale.

!!!danger "Erratum sujet 23 mars"
    - **Sujet percolation** : l'algorithme pour écrire une image zoomée était incorrect.  
    Version incorrecte :
    ```
    pour chaque colonne de l'image faire
      pour repet_ligne <- 1 à G faire
        pour chaque ligne de l'image faire
          pour repet_pixel <- 1 à G faire
            Écrire le pixel
    ```
    Version corrigée :
    ```
    pour chaque ligne de l'image faire
      pour repet_ligne <- 1 à G faire
        pour chaque colonne de l'image faire
          pour repet_pixel <- 1 à G faire
            Écrire le pixel
    ```
    Notez l'inversion des boucles `for` parcourant les lignes et les colonnes.

## Salon Riot

Lien vers le salon Riot : [lien](https://matrix.to/#/#projet_prog_22:ensimag.fr?via=ensimag.fr).
