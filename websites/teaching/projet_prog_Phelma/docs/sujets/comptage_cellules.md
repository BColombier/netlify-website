---
title: Comptage de cellules
---

## Utilitaire de génération d'images de test

L'utilitaire permettant de générer des images de test est à récupérer au lien suivant : [:fontawesome-brands-python:](../assets/generate_test_image.py).  
Il s'utilise de la manière suivante :

``` shell-session
$ python2.7 generate_test_image.py NNN NBN NNN
```
Cela génèrera l'image suivante : 

![NNNNBNNNN](../assets/comptage_cellules/NNNNBNNNN.png)

## Fichiers de test

Voici les fichiers de test à utiliser, dans cet ordre.

**Attention à bien récupérer l'image au format `pgm` et non pas `png` !**

### Images de test

#### Zones

[zones.pgm](../assets/comptage_cellules/zones.pgm)

![Zones](../assets/comptage_cellules/zones.png)

#### Disques

[disques.pgm](../assets/comptage_cellules/disques.pgm)

![Disques](../assets/comptage_cellules/disques.png)

#### Disques avec bord

[disques_bord.pgm](../assets/comptage_cellules/disques_bord.pgm)

![Disques_Bord](../assets/comptage_cellules/disques_bord.png)

#### Disques avec bord et trous

[disques_bord_trous.pgm](../assets/comptage_cellules/disques_bord_trous.pgm)

![Disques_Bord_Trous](../assets/comptage_cellules/disques_bord_trous.png)

### Images réelles

#### Cellules (très petite image)

[cellules_mini.pgm](../assets/comptage_cellules/cellules_mini.pgm)

![Cellules_Mini](../assets/comptage_cellules/cellules_mini.png)

#### Cellules (petite image)

[cellules_petit.pgm](../assets/comptage_cellules/cellules_petit.pgm)

![Cellules_Petit](../assets/comptage_cellules/cellules_petit.png)

#### Cellules (image moyenne)

[cellules.pgm](../assets/comptage_cellules/cellules.pgm)

![Cellules](../assets/comptage_cellules/cellules.png)

#### Cellules (grande image)

[cellules_grand.pgm](../assets/comptage_cellules/cellules_grand.pgm)

![Cellules_Grand](../assets/comptage_cellules/cellules_grand.png)

#### Cellules (très grande image)

[cellules_geant.pgm](../assets/comptage_cellules/cellules_geant.pgm)

![Cellules_Geant](../assets/comptage_cellules/cellules_geant.png)
