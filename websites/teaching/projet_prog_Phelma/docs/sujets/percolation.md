---
title: Percolation
---

## Utilitaire de génération d'images de test

L'utilitaire permettant de générer des images de test est à récupérer au lien suivant : [:fontawesome-brands-python:](../assets/generate_test_image.py).  
Il s'utilise de la manière suivante :

``` shell-session
$ python2.7 generate_test_image.py NNN NBN NNN
```
Cela génèrera l'image suivante : 

![NNNNBNNNN](../assets/comptage_cellules/NNNNBNNNN.png)
