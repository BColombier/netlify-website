---
hide:
  - navigation
  - toc
---
# Organisation

## Équipe pédagogique

Responsable du cours : Brice Colombier ([e-mail](mailto:brice.colombier@grenoble-inp.fr)).

### Groupe 1 (jeudi matin)

  - Dawood Al Chanti : ([e-mail](mailto:dawood.al-chanti@grenoble-inp.fr)),
  - Brice Colombier : ([e-mail](mailto:brice.colombier@grenoble-inp.fr)).

### Groupe 2 (vendredi matin)

  - Lionel Bastard : ([e-mail](mailto:lionel.bastard@grenoble-inp.fr)),
  - Éric Moisan : ([e-mail](mailto:eric.moisan@grenoble-inp.fr)).

### Groupe 3 (lundi après-midi)

  - François Cayre : ([e-mail](mailto:francois.cayre@grenoble-inp.fr)),
  - Michel Desvignes : ([e-mail](mailto:michel.desvignes@grenoble-inp.fr)).

## Travail étudiant demandé

Ce cours a une valeur de 2.5 ECTS.
Puisque 1 ECTS correspond à [25 à 30h de travail étudiant](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000028543525/#JORFARTI000028543546), vous devrez fournir **au total** environ 66h de travail.

Ces 66h de travail peuvent être réparties de la manière suivante :

  - 8$\times$4h = 32h de présence en séances de TP,
  - 2h de préparation et de passage en soutenance,
  - 32h de travail personnel sur le projet.

Il vous reste ainsi **32h de travail personnel** à fournir, soit **le même volume horaire** que les séances de TP.
Il est donc impératif que vous travailliez en autonomie sur le projet, en dehors des séances indiquées sur l'emploi du temps.
