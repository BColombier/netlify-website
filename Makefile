MAKEFLAGS+=-j16

SOURCE_HTML=$(patsubst templates/%.html.jinja, %.html, $(wildcard templates/*.html.jinja))
CONTENT=$(patsubst content/%.content.md, content/%.content.html, $(wildcard content/*.content.md))
PUBLIC_CSS=$(patsubst %.css, public/%.css, $(wildcard *.css))
PUBLIC_HTML=$(patsubst %.html, public/%.html, $(SOURCE_HTML))

all: $(SOURCE_HTML)

serve: all
	python3 -m http.server 8000 --bind 127.0.0.1

%.html: templates/%.html.jinja $(CONTENT)
	python3 render.py $< $@

content/%.content.html: content/%.content.md
	pandoc $^ -o $@

sitemap.xml: $(SOURCE_HTML)
	python3 generate_sitemap.py

public:
	rm -rf public
	mkdir public
	mkdir public/teaching

public/%.html: %.html
	./minhtml --keep-closing-tags --minify-css --output $@ $^

public/%.css: %.css
	./minhtml --keep-closing-tags --minify-css --output $@ $^

projet_prog_Phelma:
	cd websites/teaching/$@; mkdocs build
	cp -r websites/teaching/$@/site public/teaching/$@

APMAN_Phelma:
	cd websites/teaching/$@; mkdocs build
	cp -r websites/teaching/$@/site public/teaching/$@

subwebsites_teaching: projet_prog_Phelma APMAN_Phelma

prepare: all public $(PUBLIC_HTML) $(PUBLIC_CSS) sitemap.xml subwebsites_teaching
	cp -t public _headers _redirects favicon.ico robots.txt sitemap.xml
	cp -r assets/ public
	cp -r font/ public

clean:
	rm -f *.html sitemap.xml content/*.html
	rm -rf public
	rm -rf websites/*/site
