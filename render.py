from argparse import ArgumentParser
from datetime import date
from jinja2 import Environment, FileSystemLoader
import yaml

def load_yaml_data():
    with open("/home/cb64416h/Recherche/encadrement/these/doctorants.yaml") as f:
        doctorants = yaml.safe_load(f)
    with open("/home/cb64416h/Recherche/encadrement/stages/stagiaires.yaml") as f:
        stagiaires = yaml.safe_load(f)
    with open("/home/cb64416h/Recherche/projets/projects.yaml") as f:
        projects = yaml.safe_load(f)
    with open("/home/cb64416h/Recherche/biblio_perso/biblio_perso.yaml") as f:
        publications = yaml.safe_load(f)
    with open("./content/code.yaml") as f:
        code = yaml.safe_load(f)
    return doctorants, stagiaires, projects, publications, code


def load_content():
    with open("./content/index.content.html", "r") as f:
        index_content = f.read()
    with open("./content/popsci.content.html", "r") as f:
        popsci_content = f.read()
    return index_content, popsci_content

if __name__ == "__main__":

    parser = ArgumentParser(description="Render jinja template")
    parser.add_argument("template_filename")
    parser.add_argument("output_filename")
    args = parser.parse_args()
    
    env = Environment(loader=FileSystemLoader("templates/"))

    doctorants, stagiaires, projects, publications, code = load_yaml_data()
    index_content, popsci_content = load_content()

    header_template = env.get_template(f"header_footer/header.html.jinja")
    footer_template = env.get_template(f"header_footer/footer.html.jinja")

    today = date.today().strftime("%B %Y")

    template = env.get_template(args.template_filename.replace("templates/", ""))
    with open(args.output_filename, "w") as html_file:
        html_file.write(template.render(header=header_template.render(page=args.template_filename.replace("templates/", "")),
                                        footer=footer_template.render(today=today),
                                        code=code,
                                        index_content=index_content,
                                        popsci_content=popsci_content,
                                        doctorants=doctorants,
                                        stagiaires=stagiaires,
                                        projects=projects,
                                        publications=publications))
