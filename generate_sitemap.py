from pathlib import Path
import datetime

timestamp = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()

with open("sitemap.xml", "w") as sitemap:
    sitemap.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    sitemap.write('<urlset\n')
    sitemap.write('  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"\n')
    sitemap.write('  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
    sitemap.write('  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9\n')
    sitemap.write('        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n')
    for html in sorted(Path().glob("*.html")):
        sitemap.write('<url>\n')
        if str(html) == "index.html":
            sitemap.write(f'  <loc>https://bcolombier.fr/</loc>\n')
            sitemap.write(f'  <lastmod>{timestamp}</lastmod>\n')
            sitemap.write(f'  <priority>1.00</priority>\n')
        else:
            sitemap.write(f'  <loc>https://bcolombier.fr/{str(html).replace(".html", "")}</loc>\n')
            sitemap.write(f'  <lastmod>{timestamp}</lastmod>\n')
            sitemap.write(f'  <priority>0.80</priority>\n')
        sitemap.write('</url>\n')
    for pdf in sorted(Path().rglob("*.pdf")):
        sitemap.write('<url>\n')
        sitemap.write(f'  <loc>https://bcolombier.fr/{pdf}</loc>\n')
        sitemap.write(f'  <lastmod>{timestamp}</lastmod>\n')
        sitemap.write(f'  <priority>0.64</priority>\n')
        sitemap.write('</url>\n')
    sitemap.write('</urlset>')
